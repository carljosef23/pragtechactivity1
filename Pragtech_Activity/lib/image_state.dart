part of 'image_bloc.dart';

abstract class ImagesState {}

class LoadingImagesState extends ImagesState {}

class LoadedImagesState extends ImagesState {
  List <ImageModel>? images;

  LoadedImagesState ({this.images});
}

class FailedToLoadImagesState extends ImagesState {
  dynamic error;
  FailedToLoadImagesState({this.error});
}