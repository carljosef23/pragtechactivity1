import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'image_bloc.dart';
import 'image_model.dart';

class ImageView extends StatelessWidget {
  Widget build (BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text ('Images'),
      ),
      body: BlocBuilder<ImageBloc, ImagesState>(
        builder: (context, state){
          if (state is LoadingImagesState) {
            return Center (
              child:CircularProgressIndicator(),
              );
          } else if (state is LoadedImagesState){
            return RefreshIndicator(
              onRefresh: () async => BlocProvider.of<ImageBloc>(context)
              .add(PullToRefreshEvent()),
              child: ListView.builder(
                itemCount: 10,
                itemBuilder: (context, index) {
                  return Card(
                    child: ListTile(
                      title: buildImage(state.images![index]),
                    ),
                  );
                },
              ),
            );
          } else if (state is FailedToLoadImagesState){
            return Center(
              child: Text ('Error occured: ${state.error}'),
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }

  Widget buildImage(ImageModel image) {
    return Container(
      margin: EdgeInsets.all(20.0),
      padding: EdgeInsets.all(20.0),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
      ),
      child: Column(
        children: <Widget>[
          Padding(
            child: Image.network(image.url!),
            padding: EdgeInsets.only(bottom: 20),
          ),
          Text (image.title!),
        ],
      ),
     );
  }
}