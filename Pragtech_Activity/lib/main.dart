import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'image_bloc.dart';
import 'image_view.dart';



void main (){
  runApp(App());
}

class App extends StatefulWidget {
  State<StatefulWidget> createState() => AppState();
  }

  class AppState extends State<App> {
    @override
    Widget build(BuildContext context){
      return MaterialApp(
        home: BlocProvider<ImageBloc>(
          create:(_) => ImageBloc()..add(LoadImagesEvent()),
          child: ImageView(),
        ),
      );
    }
  }
