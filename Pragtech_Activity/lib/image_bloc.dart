import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import 'image_service.dart';
import 'image_model.dart';

part 'image_event.dart';
part 'image_state.dart';

class ImageBloc extends Bloc<ImagesEvent, ImagesState> {
  final service = ImageService();

  ImageBloc() : super(LoadingImagesState());

  @override
  Stream <ImagesState> mapEventToState (ImagesEvent event) async* {
    if (event is LoadImagesEvent || event is PullToRefreshEvent){
      yield LoadingImagesState();

      try{
        final images = await service.fetchImage();
        yield LoadedImagesState (images: images);
      } catch (e){
        yield FailedToLoadImagesState(error: e);
      }
    }
  }
}