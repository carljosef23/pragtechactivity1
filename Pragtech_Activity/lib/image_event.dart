part of 'image_bloc.dart';

abstract class ImagesEvent {}

class LoadImagesEvent extends ImagesEvent {}
class PullToRefreshEvent extends ImagesEvent {}
