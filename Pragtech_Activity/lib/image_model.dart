

class ImageModel {
  
  final int? id;
  final String? url;
  final String? title;

  ImageModel({this.id, this.url, this.title});

  factory ImageModel.fromJson(Map<String, dynamic> parsedJson) => ImageModel(
    id: parsedJson['id'],
    url: parsedJson['url'],
    title: parsedJson['title'],
  );
}