import 'dart:convert';
import 'package:http/http.dart' show get;
import 'image_model.dart';

class ImageService{
  final baseUrl = 'jsonplaceholder.typicode.com';

  Future<List<ImageModel>> fetchImage () async{
    try{
      final uri = Uri.https(baseUrl, '/photos');
      final response = await get(uri);
      final json = jsonDecode(response.body) as List;
      final images = json.map((postJson) => ImageModel.fromJson(postJson)).toList();

      return images;
      
    }catch (e){
      throw e;     
    }
  } 

}

